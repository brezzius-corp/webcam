package com.brezzius.webcam.activity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.StrictMode;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.brezzius.webcam.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class DashboardActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        ArrayList<ImageView> image = new ArrayList<>();
        ArrayList<String> surl = new ArrayList<>();

        image.add(findViewById(R.id.luchon_plateau));
        image.add(findViewById(R.id.luchon_cecire));
        image.add(findViewById(R.id.besurta_nord));
        image.add(findViewById(R.id.besurta_sud));

        surl.add("https://live.neos360.com/luchon/plateau/panojavascript/pano_PV.jpg");
        surl.add("https://live.neos360.com/luchon/cecire/panojavascript/pano_PV.jpg");
        surl.add("https://prames.es/centralreservas/webrefugios/camaras/renclusab.jpg");
        surl.add("https://prames.es/centralreservas/webrefugios/camaras/renclusa.jpg");

        try {
            for(int i = 0, l = image.size() ; i < l ; i++) {
                URL url = new URL(surl.get(i));
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();

                if(i == 2 || i == 3)
                    conn.setRequestProperty("Referer", "https://www.alberguesyrefugios.com/larenclusa");

                conn.connect();
                int resCode = conn.getResponseCode();

                if (resCode == HttpURLConnection.HTTP_OK) {
                    InputStream in = conn.getInputStream();
                    Bitmap bitmap = BitmapFactory.decodeStream(in);

                    image.get(i).setImageBitmap(bitmap);
                }
            }

        } catch(IOException e) {
            e.printStackTrace();
        }
    }
}
